""" Student functions """
from Student import Students

""" the Json library will allow us create a json file were we going to insert the users """
import json

""" Function that will validate if we going to continue adding users on the system """
def KeepAdding():
   keep_adding = int(input("Kepp adding users? 1: yes, 2: no "))
   if keep_adding == 1:
       return True
   elif keep_adding == 2:
       return False
   else:
       KeepAdding()

""" Function to add users on the system """
def AddStudent():
    Student = Students
    Student.name = input("Insert Name: ")
    Student.age =  int(input("Insert Age: "))
    Student.course = input("Insert Course: ")
    DicEst = {'Name': Student.name, 'Age': Student.age, 'Course': Student.course}

    """ On this section we create the json file from the scratch and insert the user created DicEst """
    tf = open("list.json", 'a')
    json.dump(DicEst,tf)
    tf.write("\n")
    tf.close()

    """
    Once the user is create we validate if the admin want to keep adding users if
    so the AddStudent function will be called if not the program is finished
    """
    if KeepAdding():
       AddStudent()
    else:
        print("Program concluided" )